<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>Labyrinth</title>
  <meta http-equiv="refresh" content="2">
</head>
<body style="background-color: #fffbdb">

<script>
  function f(x, y) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/start?x=' + x + '&y=' + y);
    xhr.send();
    location.reload();
  }
</script>

<style>
  th {
    padding: 10px 20px;
    background: #56433D;
    color: #F9C941;
    border-right: 2px solid;
    font-size: 0.9em;
  }
</style>

<table style="height: 10vh; display:flex; justify-content: center; align-items: center; align-content: center">
  <tr>
    <th>${player1}</th>
    <th>${player2}</th>
  </tr>
  <tr>
    <td align="center" width="100px" style=" border: 1px solid black; color: #FF6600">${xCount}</td>
    <td align="center" width="100px" style=" border: 1px solid black; color: #008DD2">${oCount}</td>
  </tr>
</table>

<table style="border: none; display:flex; justify-content: center; align-items: center; align-content: center">
  <#assign i = 0>
  <#list attributes as row>
    <tr>
      <#assign j = 0>
      <#list row as cell>
        <#if i % 2 == 0 && j % 2 == 0>
          <td width="3px" height="3px" style="background-color: ${cell}">
          </td>
        <#elseif i % 2 == 1 && j % 2 == 1>
          <td width="15px" height="15px" style="background-color: white" align="center" valign="middle">
            <img src="${cell}" width="13px" height="13px">
          </td>
        <#elseif i % 2 == 0 && j % 2 == 1>
          <td width="15px" height="3px" style="background-color: ${cell}" onclick="f(${i}, ${j})">
          </td>
        <#elseif i % 2 == 1 && j % 2 == 0>
          <td width="3px" height="15px" style="background-color: ${cell}" onclick="f(${i}, ${j})">
          </td>
        </#if>
        <#assign j++>
      </#list>
    </tr>
    <#assign i++>
  </#list>
</table>
</body>
</html>
