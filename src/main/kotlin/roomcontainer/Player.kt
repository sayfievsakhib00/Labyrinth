package roomcontainer

class Player(val name: String) {
  val id = getId()

  companion object {
    private var nextId = 0
    private fun getId(): Int {
      nextId++
      return nextId
    }
  }
}
