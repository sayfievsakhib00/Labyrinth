package playground.cell

enum class BorderColor(val value: String) {
  GRAY("#D8D9D9"),
  BLACK("#2B2A29"),
  BLUE("#008DD2"),
  ORANGE("#FF6600"),
  BEIGE("#FFFBDB")
}
