package playground

import playground.cell.BorderColor
import playground.cell.Cell
import playground.cell.CellContent

class Playground(height: Int, width: Int) {
  val vSize = 2 * height + 1
  val hSize = 2 * width + 1
  val table = Array(vSize) { Array(hSize) { Cell() } }
  var xCount = 0
  var oCount = 0
  var emptyCells = height * width

  init {
    for (i in 0 until vSize) {
      for (j in 0 until hSize) {
        if (i == 0 || j == 0 || i == vSize - 1 || j == hSize - 1) {
          table[i][j].color = BorderColor.BLACK
        } else if (i % 2 == 0 && j % 2 == 1 || i % 2 == 1 && j % 2 == 0) {
          table[i][j].color = BorderColor.GRAY
        }
      }
    }
  }

  fun changeBorderColor(x: Int, y: Int, player: Int): Int {
    if (table[x][y].color != BorderColor.GRAY)
      return -1
    when (player) {
      0 -> table[x][y].color = BorderColor.ORANGE
      1 -> table[x][y].color = BorderColor.BLUE
    }
    val first: Boolean
    val second: Boolean
    if (x % 2 == 0) {
      first = fillCell(x - 1, y, player)
      second = fillCell(x + 1, y, player)
    } else {
      first = fillCell(x, y - 1, player)
      second = fillCell(x, y + 1, player)
    }
    return if (first || second) 1 else 0
  }

  private fun fillCell(x: Int, y: Int, player: Int): Boolean {
    if (table[x][y].image != CellContent.EMPTY) return false

    if (table[x - 1][y].color != BorderColor.GRAY &&
      table[x + 1][y].color != BorderColor.GRAY &&
      table[x][y - 1].color != BorderColor.GRAY &&
      table[x][y + 1].color != BorderColor.GRAY
    ) {
      when (player) {
        0 -> {
          table[x][y].image = CellContent.X
          xCount++
        }
        1 -> {
          table[x][y].image = CellContent.O
          oCount++
        }
      }
      emptyCells--
      return true
    }
    return false
  }
}
