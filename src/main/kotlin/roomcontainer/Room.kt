package roomcontainer

import playground.Playground

class Room(val board: Playground, host: Player) {
  val players = mutableListOf(host)

  var isGameStarted = false
    private set
  var turn = 0                   //номер ирока, делающего следующий ход
  var isGameFinished = false
  var id = getId()

  companion object {
    private var nextId = 0
    private fun getId(): Int {
      nextId++
      return nextId
    }
  }

  fun addGuest(guest: Player): Boolean {
    if (players.size == 1)
      players.add(guest)
    else return false
    isGameStarted = true
    return true
  }
}
