package playground.cell

enum class CellContent(val link: String) {
  X("icons/X.png"),
  O("icons/O.png"),
  EMPTY("icons/square.png")
}
