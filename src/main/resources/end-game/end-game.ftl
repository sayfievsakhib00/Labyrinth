<!DOCTYPE html>
<html lang="ru">
<head>

  <title>Labyrinth</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link rel="stylesheet" href="../../css/bootstrap.min.css">
  <link rel="stylesheet" href="../../css/font-awesome.min.css">
  <link rel="stylesheet" href="../../css/aos.css">

  <link rel="stylesheet" href="../../css/tooplate-gymso-style.css">

</head>
<body data-spy="scroll" data-target="#navbarNav" data-offset="50" style = "background-color: #fffbdb">

<!-- HERO -->
<section class="hero d-flex flex-column justify-content-center align-items-center" id="home">

  <div class="bg-overlay"></div>
  <img src="${gif}">
  <div class="container">
    <div class="row">

      <div class="col-lg-8 col-md-10 mx-auto col-12">
        <div class="hero-text mt-5 text-center">

             <h1 class="text-white" data-aos="fade-up" data-aos-delay="500">${winnerName}</h1>
        </div>
      </div>

    </div>
  </div>
</section>

<h2 id="footer"><a href="/delete-room">На главную &raquo;</a></h2>

<!-- SCRIPTS -->
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/aos.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/custom.js"></script>

</body>
</html>
