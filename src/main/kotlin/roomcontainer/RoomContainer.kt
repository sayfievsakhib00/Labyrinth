package roomcontainer

import playground.Playground

object RoomContainer {
  val roomList = mutableListOf<Room>()

  fun addRoom(name: String, height: Int, width: Int): Pair<Int, Int> {
    val player = Player(name)
    val room = Room(Playground(height, width), player)
    roomList.add(room)
    return Pair(player.id, room.id)
  }

  fun deleteRoom(id: Int) {
    for (i in 0 until roomList.size)
      if (roomList[i].id == id) {
        roomList.removeAt(i)
        return
      }
  }
}
