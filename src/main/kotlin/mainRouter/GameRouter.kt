package mainRouter

import controller.GameController
import io.vertx.core.Vertx
import io.vertx.core.http.CookieSameSite
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.sstore.LocalSessionStore

class GameRouter {

  fun getRouter(vertx: Vertx): Router {
    val gameController = GameController(vertx)
    val router = Router.router(vertx)
    val sessionStore = LocalSessionStore.create(vertx)
    val sessionHandler = SessionHandler.create(sessionStore)
    sessionHandler.setCookieSameSite(CookieSameSite.STRICT)

    router.route().handler(sessionHandler)

    router.get("/").handler(StaticHandler.create().setWebRoot("html").setIndexPage("HomePage.html"))
    router.get("/create-room").handler(gameController::handleCreatingRoom)
    router.get("/create-room").handler(StaticHandler.create().setWebRoot("create-room").setIndexPage("CreateRoom.html"))
    router.get("/select-room").handler(gameController::gameJoiningHandler)
    router.get("/select-room").handler(gameController::handleChoosingPage)

    router.route("/waiting").handler(gameController::waitingHandler)
    router.route("/waiting")
      .handler(StaticHandler.create().setWebRoot("waiting-room").setIndexPage("html/WaitingRoom.html"))

    router.route("/start").handler(gameController::gameHandler)
    router.get("/icons/*").handler(StaticHandler.create().setWebRoot("icons"))

    router.get("/endgame").handler(gameController::endgameHandler)
    router.get("/gifs/*").handler(StaticHandler.create().setWebRoot("gifs"))
    router.get("/delete-room").handler(gameController::deleteRoomHandler)

    router.get("/css/*").handler(StaticHandler.create().setWebRoot("css"))
    router.get("/js/*").handler(StaticHandler.create().setWebRoot("js"))
    router.get("/fonts/*").handler(StaticHandler.create().setWebRoot("fonts"))
    router.get("/create-room/*").handler(StaticHandler.create().setWebRoot("create-room"))
    router.get("/waiting-room/*").handler(StaticHandler.create().setWebRoot("waiting-room"))

    return router
  }
}
