<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Labyrinth</title>

<link rel="stylesheet" type="text/css" href="../../resources/select-room/styles.css" />

  <style type="text/css">
    *{
      /* Resetting the default styles of the page */
      margin:0;
      padding:0;
    }

    html{
      overflow:auto;
    }

    body{
      /* Setting default text color, background and a font stack */
      font-size:0.825em;
      color:#eee;
      background:url("img/bg.png") repeat-x #222222;
      font-family:Arial, Helvetica, sans-serif;
    }

    #carbonForm{
      /* The main form container */
      background-color:#1C1C1C;
      border:1px solid #080808;
      margin:20px auto;
      padding:20px;
      width:550px;

      -moz-box-shadow:0 0 1px #444 inset;
      -webkit-box-shadow:0 0 1px #444 inset;
      box-shadow:0 0 1px #444 inset;
    }

    #carbonForm h1{
      /* The form heading */
      font-family:Century Gothic,Myriad Pro,Arial,Helvetica,sans-serif;
      font-size:45px;
      font-weight:normal;
      padding:0 0 30px 10px;
      text-align:center;
    }

    .fieldContainer{
      /* The light rounded section, which contans the fields */
      background-color:#1E1E1E;
      border:1px solid #0E0E0E;
      padding:30px 10px;

      /* CSS3 box shadow, used as an inner glow */
      -moz-box-shadow:0 0 20px #292929 inset;
      -webkit-box-shadow:0 0 20px #292929 inset;
      box-shadow:0 0 20px #292929 inset;
    }

    #carbonForm,.fieldContainer,.errorTip{
      /* Rounding the divs at once */
      -moz-border-radius:12px;
      -webkit-border-radius:12px;
      border-radius:12px;
    }


    .formRow{
      height:35px;
      padding:10px;
      position:relative;
    }

    .label{
      float:left;
      padding:0 20px 0 0;
      text-align:right;
      width:120px;
    }

    label{
      font-family:Century Gothic,Myriad Pro,Arial,Helvetica,sans-serif;
      font-size:14px;
      letter-spacing:1px;
      line-height:35px;
    }

    .field{
      float:left;
    }

    .field input{
      /* The text boxes */
      border:1px solid white;
      color:#666666;
      font-family:Arial,Helvetica,sans-serif;
      font-size:18px;
      padding:4px 5px;
      background:url("img/box_bg.png") repeat-x scroll left top #FFFFFF;
      outline:none; /* Preventing the default Safari and Chrome text box highlight */
    }

    .signupButton{
      /* The submit button container */
      text-align:center;
      padding:30px 0 10px;
    }

    #submit{
      /* The submit button */
      border:1px solid #f4f4f4;
      cursor:pointer;
      height:40px;
      text-indent:-9999px;
      text-transform:uppercase;
      width:110px;

      background:url("img/submit.png") no-repeat center center #d0ecfd;

      -moz-border-radius:6px;
      -webkit-border-radius:6px;
      border-radius:6px;
    }

    #submit.active{
      /* Marking the submit button as active adds the preloader gif as background */
      background-image:url("img/preloader.gif");
    }

    #submit:hover{
      background-color:#dcf2ff;
      border:1px solid white;
    }

    input:hover,
    input:focus{
      -moz-box-shadow:0 0 8px lightblue;
      -webkit-box-shadow:0 0 8px lightblue;
      box-shadow:0 0 8px lightblue;
    }

    .errorTip{
      /* The error divs */
      background-color:#970F08;
      color:white;
      font-size:10px;
      height:26px;
      letter-spacing:0.4px;
      margin-left:20px;
      padding:5px 0 5px 10px;
      position:absolute;
      text-shadow:1px 1px 0 #555555;
      width:200px;
      right:-130px;
    }


    /* The styles below are only necessary for the styling of the demo page: */

    #footer{
      position:fixed;
      bottom:0;
      width:100%;
      padding:10px;
      color:#eee;
      text-align:center;
      font-weight:normal;
      font-style:italic;
    }

    a, a:visited {
      color:#0196e3;
      text-decoration:none;
      outline:none;
    }

    a:hover{
      text-decoration:underline;
    }

    a img{
      border:none;
    }

    th {
      padding: 10px 20px;
      background: #56433D;
      color: #F9C941;
      border-right: 2px solid;
      font-size: 0.9em;
    }
  </style>

</head>
<#assign i = 1>
<body>

<div id="carbonForm">
	<h1>Выберите комнату</h1>

  <table style="display:flex; justify-content: center; align-items: center; align-content: center">
    <tr>
      <th>№</th> <th>Имя владельца</th> <th>ID комнаты</th>
    </tr>
    <#list userMap as host, id>
      <tr>
        <td align="center" width="100px" style=" border: 1px solid black"> ${i} </td>
        <td align="center" width="100px" style=" border: 1px solid black">${host}</td>
        <td align="center" width="100px" style=" border: 1px solid black">${id}</td>
      </tr>
      <#assign i++>
    </#list>
  </table>

    <form method="get" id="signupForm">

    <div class="fieldContainer">

        <div class="formRow">
            <div class="label">
                <label for="name">Имя:</label>
            </div>

            <div class="field">
                <input type="text" required name="name" id="name" />
            </div>
        </div>

        <div class="formRow">
            <div class="label">
                <label for="RoomId">ID комнаты:</label>
            </div>

            <div class="field">
                <input type="text" required name="RoomId" id="RoomId" />
            </div>
        </div>

    </div> <!-- Closing fieldContainer -->

    <div class="signupButton">
        <input type="submit" name="submit" id="submit" value="Signup" />
    </div>



    </form>



</div>

<h2 id="footer"><a href="/">Назад &raquo;</a></h2>

<script type="text/javascript">
    $(window).resize(function(){
        const cf = $('#carbonForm');
        $('#carbonForm').css('margin-top',($(window).height()-cf.outerHeight())/2)
    });
</script>

</body>
</html>
