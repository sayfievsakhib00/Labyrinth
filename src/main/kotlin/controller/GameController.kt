package controller

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine
import roomcontainer.Player
import roomcontainer.RoomContainer

class GameController(vertx: Vertx) {

  private val templateEngine: FreeMarkerTemplateEngine = FreeMarkerTemplateEngine.create(vertx)

  fun handleChoosingPage(routingContext: RoutingContext) {
    val response = routingContext.response()
    val userMap = mutableMapOf<String, Int>()

    for (item in RoomContainer.roomList) {
      if (!item.isGameStarted)
        userMap[item.players[0].name] = item.id
    }

    val data = JsonObject().put("userMap", userMap)

    templateEngine.render(data, "select-room/selectRoom.ftl") {
      response.putHeader("content-type", "text/html").end(it.result())
    }
  }

  fun handleCreatingRoom(routingContext: RoutingContext) {
    try {
      routingContext.request().getParam("submit")

      val session = routingContext.session()
      val hostName = routingContext.request().getParam("name")
      val tableHeight = routingContext.request().getParam("height").toInt()
      val tableWidth = routingContext.request().getParam("width").toInt()

      val (player, room) = RoomContainer.addRoom(hostName, tableHeight, tableWidth)
      session.put("playerID", player)
      session.put("roomID", room)

      routingContext.response().putHeader("Location", "/waiting").setStatusCode(302).end()
    } catch (e: Exception) {
      routingContext.next()
    }
  }

  fun waitingHandler(routingContext: RoutingContext) {

    val session = routingContext.session()
    for (room in RoomContainer.roomList) {
      if (room.id == session["roomID"]) {
        if (room.isGameStarted)
          routingContext.response().putHeader("Location", "/start?x=0&y=0").setStatusCode(302).end()
        else break
      }
    }
    routingContext.next()
  }

  fun gameJoiningHandler(routingContext: RoutingContext) {
    try {
      val name = routingContext.request().getParam("name")
      val roomId = routingContext.request().getParam("RoomId").toInt()
      val session = routingContext.session()

      var flag = false
      for (i in 0 until RoomContainer.roomList.size) {
        if (roomId == RoomContainer.roomList[i].id && !RoomContainer.roomList[i].isGameStarted) {
          flag = true
          val guest = Player(name)
          RoomContainer.roomList[i].addGuest(guest)
          session.put("playerID", guest.id)
          session.put("roomID", roomId)
          break
        }
      }
      if (!flag) throw Exception()

      routingContext.response().putHeader("Location", "/start?x=0&y=0").setStatusCode(302).end()

    } catch (e: Exception) {
      routingContext.next()
    }
  }

  fun gameHandler(routingContext: RoutingContext) {
    val x = routingContext.request().getParam("x").toInt()
    val y = routingContext.request().getParam("y").toInt()
    val session = routingContext.session()
    val playerId = session.get<Int>("playerID")
    val roomId = session.get<Int>("roomID")
    val currentRoom = RoomContainer.roomList.find { it.id == roomId }!!

    if (currentRoom.isGameFinished) {
      routingContext.response().putHeader("Location", "/endgame").setStatusCode(302).end()
    }

    if (playerId == currentRoom.players[currentRoom.turn].id) {
      val isTurnChanged = currentRoom.board.changeBorderColor(x, y, currentRoom.turn)
      if (isTurnChanged == 0)
        currentRoom.turn = (currentRoom.turn + 1) % 2
      if (currentRoom.board.emptyCells == 0)
        currentRoom.isGameFinished = true
    }

    val table = JsonObject()
    val height = currentRoom.board.vSize
    val width = currentRoom.board.hSize
    table.put("player1", currentRoom.players[0].name)
    table.put("player2", currentRoom.players[1].name)
    table.put("xCount", currentRoom.board.xCount)
    table.put("oCount", currentRoom.board.oCount)
    val array = Array(height) { Array(width) { "" } }
    for (i in 0 until height) {
      for (j in 0 until width) {
        if (i % 2 == 1 && j % 2 == 1)
          array[i][j] = currentRoom.board.table[i][j].image.link
        else
          array[i][j] = currentRoom.board.table[i][j].color.value
      }
    }
    table.put("attributes", array)
    templateEngine.render(table, "ftl/table.ftl") {
      if (it.succeeded()) {
        routingContext.response().putHeader("content-type", "text/html").end(it.result())
      }
    }
  }

  fun endgameHandler(routingContext: RoutingContext) {
    val session = routingContext.session()
    val roomId = session.get<Int>("roomID")
    val currentRoom = RoomContainer.roomList.find { it.id == roomId }!!

    val data = JsonObject()

    val winnerName = when {
      currentRoom.board.xCount > currentRoom.board.oCount -> "Победил(а) ${currentRoom.players[0].name}"
      currentRoom.board.xCount < currentRoom.board.oCount -> "Победил(а) ${currentRoom.players[1].name}"
      else -> "Победила ничья"
    }

    val gif = when {
      currentRoom.board.xCount > currentRoom.board.oCount -> "gifs/orange.gif"
      currentRoom.board.xCount < currentRoom.board.oCount -> "gifs/blue.gif"
      else -> "gifs/draw.gif"
    }

    data.put("winnerName", winnerName)
    data.put("gif", gif)

    templateEngine.render(data, "end-game/end-game.ftl") {
      if (it.succeeded()) {
        routingContext.response().putHeader("content-type", "text/html").end(it.result())
      }
    }
  }

  fun deleteRoomHandler(routingContext: RoutingContext) {
    val session = routingContext.session()
    val roomId = session.get<Int>("roomID")
    RoomContainer.deleteRoom(roomId)
    routingContext.response().putHeader("Location", "/").setStatusCode(302).end()
  }
}
